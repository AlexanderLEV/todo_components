export default class CardService {
  constructor($http) {
    'ngInject';
    this.$http = $http;
    this.swalConfiguration = {
      title: 'Are you sure?',
      text: 'You will not be able to recover this card!',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      closeOnConfirm: false
    };
  }

  list() {
    return this.$http.get(`${API_URL}/cards`).then(result => result.data);
  }

  add() {
    const card = {
      title: 'Task',
      color: this.getRandomRgbColor(),
      todos: []
    };
    return this.$http.post(`${API_URL}/cards/`, card).then(result => result.data);
  }

  update(card) {
    return this.$http.put(`${API_URL}/cards/${card.id}`, card).then(result => result.data);
  }

  remove(id) {
    return this.$http.delete(`${API_URL}/cards/${id}`);
  }

  checkAndUpdate(str, card) {
    if (this.isEmpty(str)) {
      throw new Error('Todo can not be empty!');
    }
    return this.update(card);
  }

  getRandomRgbColor() {
    return `rgb(${this.getRandomNumber()}, ${this.getRandomNumber()}, ${this.getRandomNumber()})`;
  }

  getRandomNumber() {
    return Math.floor(Math.random() * 256);
  }

  isEmpty(str) {
    return !str;
  }

  allTodosComplete(card) {
    card.$allComplete = card.todos.every(item => item.isComplete) && card.todos.length > 0;
  }
}
