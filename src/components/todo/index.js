import ng from 'angular';
import TodoComponent from './todo.component';

export default ng.module('todo.component.app', [])
  .component('todo', TodoComponent)
  .name;
