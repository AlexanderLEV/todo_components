import template from './cardList.html';
import controller from './cardList.controller';
import style from './style.less';

export default {
  template,
  controller,
  style
};
