import ng from 'angular';
import CardListComponent from './cardList.component';

export default ng.module('cardList.components.app', [])
  .component('cardList', CardListComponent)
  .name;
