export default class CardListController {
  constructor(CardService) {
    'ngInject';
    this.CardService = CardService;
  }

  $onInit() {
    this.init();
  }

  init() {
    this.CardService.list().then((cards) => {
      this.cards = cards;
      this.addCardBtnIsShow = cards.length < 3;
      this.cards.forEach(item => this.CardService.allTodosComplete(item));
    });
  }

  removeCard(id) {
    swal(this.CardService.swalConfiguration, () => {
      this.CardService.remove(id).then(this.init.bind(this));
      swal({ title: 'Deleted!', text: 'Your card has been deleted.', timer: 500, showConfirmButton: false });
      return true;
    });
  }

  addCard() {
    this.CardService.add().then(this.init.bind(this));
  }
}
