import ng from 'angular';
import CardComponent from './card.component';

export default ng.module('card.components.app', [])
  .component('card', CardComponent)
  .name;
