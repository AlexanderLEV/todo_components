export default class CardController {
  constructor(CardService) {
    'ngInject';
    this.CardService = CardService;
  }

  $onInit() {
    this.init();
  }

  init() {
    this.CardService.allTodosComplete(this.card);
    this.todoTitle = '';
  }

  removeCard() {
    this.removeCard();
  }

  addTodo() {
    try {
      this.CardService.checkAndUpdate(this.todoTitle, this.card).then(this.init.bind(this));
      this.card.todos.push({
        title: this.todoTitle,
        isComplete: false,
      });
    } catch (ex) {
      swal('Error!', `${ex}`, 'error');
    }
  }

  removeTodo(id) {
    swal(this.CardService.swalConfiguration, () => {
      this.card.todos.splice(id, 1);
      this.CardService.update(this.card).then(this.init.bind(this));
      swal({ title: 'Deleted!', text: 'Your todo has been deleted.', timer: 500, showConfirmButton: false });
      return true;
    });
  }

  editTodo(id) {
    try {
      this.CardService.checkAndUpdate(this.card.todos[id].title, this.card);
      delete this.card.todos[id].$edit;
    } catch (ex) {
      swal('Error!', `${ex}`, 'error');
    }
  }

  isComplete(id) {
    this.card.todos[id].isComplete = !this.card.todos[id].isComplete;
    this.CardService.update(this.card).then(this.init.bind(this));
  }
}

