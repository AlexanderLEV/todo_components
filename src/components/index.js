import ng from 'angular';

import CardList from './cardList';
import Card from './card';
import Todo from './todo';
import CardService from '../services/card.service';

export default ng.module('app.components', [Card, CardList, Todo])
  .service('CardService', CardService)
  .name;
