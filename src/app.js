import 'bootstrap/less/bootstrap.less';
import 'font-awesome/less/font-awesome.less';

import 'sweetalert/dist/sweetalert.min.js';
import 'sweetalert/dist/sweetalert.css';
import 'sweetalert/themes/google/google.css';

import ng from 'angular';
import ngComponents from  './components';

ng.module('app', [ngComponents])